import React, { Fragment } from 'react'
import { Route } from 'react-router-dom'
import Notifications from 'react-notification-system-redux'
import { connect } from 'react-redux'
import LoadingScreenTransparent from '../../components/common/LoadingScreenTransparent'

const PublicLayout = ({ notifications, isLoading, component: Component, ...rest }) => (
    <Route 
        {...rest}
        render={
            props => <Fragment>
                <Component {...props} />
                <Notifications notifications={notifications} />
                {isLoading && <LoadingScreenTransparent/>}
            </Fragment>
        }
    />
)

const mapStateToProps = (state) => ({
    notifications: state.notifications,
    isLoading: state.loading.isLoading
})

export default connect(mapStateToProps)(PublicLayout)