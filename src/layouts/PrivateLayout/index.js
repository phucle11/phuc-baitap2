import React, { Fragment } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import Notifications from 'react-notification-system-redux'
import LoadingScreenTransparent from '../../components/common/LoadingScreenTransparent'

const PrivateRoute = ({isAuthenticated, notifications, isLoading, component: Component, ...rest}) => (
    <Route 
        {...rest}
        render={
            props => isAuthenticated? <Fragment>
                <Component {...props} />
                <Notifications notifications={notifications} />
                {isLoading && <LoadingScreenTransparent />}
            </Fragment>
            : <Redirect to="/login/" />
        }
    />
)

const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    notifications: state.notifications,
    isLoading: state.loading.isLoading
})

export default connect(mapStateToProps)(PrivateRoute)