import _ from 'lodash'
import { call, put } from 'redux-saga/effects'
import { changeLoadingStatus } from './loading/actions'

export function makeConstantCreator(...params) {
    const constant = {}
    _.each(params, param => {
        constant[param] = param
    })
    return constant
}

export const makeActionCreator = (type, params = null) => ({
    type,
    ...params
})

export const makeReducerCreator = (initialState = null, handlers = {}) => (
    state = initialState,
    action
) => {
    if(!action && !action.type) return state
    const handler = handlers[action.type]
    return (handler && handler(state,action)) ||state
}

export function* apiWrapper(options, apiFunction, ...params) {
    try {
        const response = yield call(apiFunction, ...params)
        return response
    }
    catch(e) {
        yield put(changeLoadingStatus(false))
    }
}