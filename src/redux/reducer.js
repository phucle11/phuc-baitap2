import { combineReducers } from 'redux'
import auth from './auth/reducers'
import loading from './loading/reducers'
import users from './users/reducers'
import { reducer as notifications } from 'react-notification-system-redux'

export default combineReducers({
    auth,
    loading,
    users,
    notifications
})
