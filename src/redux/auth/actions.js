import { makeConstantCreator, makeActionCreator } from '../reduxCreator'

export const AuthTypes = makeConstantCreator(
    'LOGIN_REQUEST',
    'LOGIN_SUCCESS',
    'LOGIN_FAIL',
    'AUTHENTICATE_USER',
    'LOGOUT'
)

export const loginRequest = ({email, password}) => makeActionCreator(AuthTypes.LOGIN_REQUEST, {email, password})

export const loginSuccess = () => makeActionCreator(AuthTypes.LOGIN_SUCCESS)

export const loginFail = () => makeActionCreator(AuthTypes.LOGIN_FAIL)

export const authenticateUser = () => makeActionCreator(AuthTypes.AUTHENTICATE_USER)

export const logout = () => makeActionCreator(AuthTypes.LOGOUT)