import { takeEvery, put, call } from 'redux-saga/effects';
import { loginSuccess, loginFail, AuthTypes } from '../auth/actions'
import { postAPI } from '../../api/api'
import { success, error } from 'react-notification-system-redux'
import { changeLoadingStatus } from '../loading/actions'
import { apiWrapper } from '../reduxCreator'

const status = (val) => ({
  status: val
})

function* loginSaga({email, password}){
  try {
    console.log(email, password)
    yield put(changeLoadingStatus(status(true)))
    const data = yield call(apiWrapper, null, postAPI, '/auth/login', {email, password})
    if(data && data.token) {
      localStorage.setItem('token', data.token)
      yield put(loginSuccess())
      yield put(
        success({
          title: 'Done',
          message: 'Login sucessfully',
          autoDismiss: 3,
        })
      )
    }
    else {
      yield put(loginFail())
      yield put(
        error({
          title: 'Oops',
          message: 'Account does not exist',
          position: 'tr',                  
          autoDismiss: 3,
        })
      )
    }
    yield put(changeLoadingStatus(status(false)))
  }
  catch(e) {
    yield put(loginFail())
    yield put(
      error({
        title: 'Oops',
        message: 'Account does not exist',
        position: 'tr',                  
        autoDismiss: 3,
      })
    )
    yield put(changeLoadingStatus(status(false)))
  }
}

export default [takeEvery(AuthTypes.LOGIN_REQUEST, loginSaga)]