import { AuthTypes } from './actions'
import { makeReducerCreator } from '../reduxCreator'

export const initialState = {
    isAuthenticated: false,
}

const loginSuccess = (state) => ({
    ...state,
    isAuthenticated: true
})

const loginFail = (state) => ({
    ...state,
    isAuthenticated: false
})

const authenticateUser = (state) => ({
    ...state,
    isAuthenticated: true
})

const logout = (state) => (
    localStorage.removeItem('token'),
    {
    ...state,
    isAuthenticated: false
})

const auth = makeReducerCreator(initialState, {
    [AuthTypes.LOGIN_SUCCESS]: loginSuccess,
    [AuthTypes.LOGIN_FAIL]: loginFail,
    [AuthTypes.AUTHENTICATE_USER]: authenticateUser,
    [AuthTypes.LOGOUT]: logout
})

export default auth