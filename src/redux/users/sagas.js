import { takeEvery, put, call } from 'redux-saga/effects';
import { getUserSuccess, getUserFail } from './actions'
import { getUserRequestSaga, getAPI } from '../../api/api'
import { changeLoadingStatus } from '../loading/actions'
import { UsersTypes } from './actions'
import { apiWrapper } from '../reduxCreator'

const status = (val) => ({
    status: val
})

function* getUserSaga(){
    try{
        yield put(changeLoadingStatus(status(true)))
        const data = yield call(apiWrapper, null, getAPI, '/users?limit=16&offset=0')
        console.log(data)
        if(data && data.results) {
            yield put(getUserSuccess({users: data.results}))
        }
        else {
            yield put(getUserFail())
        }
        yield put(changeLoadingStatus(status(false)))
    }
    catch(e) {
        yield put(changeLoadingStatus(status(false)))
    }
}

export default [takeEvery(UsersTypes.GET_USER_REQUEST, getUserSaga)]