import { UsersTypes } from './actions'
import { makeReducerCreator } from '../reduxCreator'

export const initialState = []

const getUserSuccess = (state, action) => ([
    ...state,
    ...action.users
])

const users = makeReducerCreator(initialState, {
    [UsersTypes.GET_USER_SUCCESS]: getUserSuccess
})

export default users