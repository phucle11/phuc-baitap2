import { makeConstantCreator, makeActionCreator } from '../reduxCreator'

export const UsersTypes = makeConstantCreator (
    'GET_USER_REQUEST',
    'GET_USER_SUCCESS',
    'GET_USER_FAIL'
)

export const getUserRequest = () => makeActionCreator(UsersTypes.GET_USER_REQUEST)

export const getUserSuccess = ({users}) => makeActionCreator(UsersTypes.GET_USER_SUCCESS, {users})

export const getUserFail = () => makeActionCreator(UsersTypes.GET_USER_FAIL)