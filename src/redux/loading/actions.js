import { makeConstantCreator, makeActionCreator } from '../reduxCreator'

export const LoadingTypes = makeConstantCreator (
    'CHANGE_LOADING_STATUS'
)

export const changeLoadingStatus = ({status}) => makeActionCreator(LoadingTypes.CHANGE_LOADING_STATUS, {status})
