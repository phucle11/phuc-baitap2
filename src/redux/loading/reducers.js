import { makeReducerCreator } from '../reduxCreator'
import { LoadingTypes } from './actions'

export const initialState = {
    isLoading: false
}

export const changeLoadingStatus = (state, action) => ({
    ...state,
    isLoading: action.status
})

const loading = makeReducerCreator (initialState, {
    [LoadingTypes.CHANGE_LOADING_STATUS]: changeLoadingStatus
})

export default loading