import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { getUserRequest } from '../../redux/users/actions'
import { logout } from '../../redux/auth/actions'
import { Table } from 'antd'
import './ListItem.css'

import 'antd/dist/antd.css';
const columns = [
    {
        title: 'Fullname',
        dataIndex: 'fullName',
        key: 'fullName'
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email'
    },
    {
        title: 'Id',
        dataIndex: 'id',
        key: 'id'
    },
]

const ListItem = ({users,  getUserRequest, logout}) => {
    const onLogout = e => {
        e.preventDefault()
        logout()
    }
    useEffect(() => {
        users.length === 0 && getUserRequest()
    }, [])
    return (
        <div id="table-wrapper">
            <Table pagination={{ defaultPageSize: 10 }} columns={columns} dataSource={users} />
            <button style={{float: "right"}} type="button" onClick={onLogout}>Logout</button>
        </div>
    )
}

const mapStateToProps = (state) => ({
    users: state.users
})

const mapDispatchToProps = (dispatch) => ({
    getUserRequest: () => dispatch(getUserRequest()),
    logout: () => dispatch(logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(ListItem)