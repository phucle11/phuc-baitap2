import React from 'react'
import { connect } from 'react-redux'
import { loginRequest } from '../../redux/auth/actions'
import { Redirect } from 'react-router-dom'
import './Login.css'
import 'antd/dist/antd.css'
import { Form, Icon, Input, Button, Checkbox } from 'antd'

const FormItem = Form.Item

const Login = ({isAuthenticated, form, loginRequest}) => {
    const { getFieldDecorator } = form
    const onSubmit = (e) => {
        e.preventDefault()
        form.validateFields((err, values) => {
            if(!err)
                loginRequest(values)
        })
    }
    return (
        <div id="login-page">
            <Form onSubmit={onSubmit} className="login-form">
                <div id="formWrapper">
                    <header>LOGIN</header>
                    <FormItem>
                        {getFieldDecorator('email', {
                            rules: [{ required: true, message: 'Please input email'}]
                        })(
                            <Input prefix={<Icon type="user" /> } placeholder="email" style={{ color: 'rgba(0,0,0,.25)' }} />
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'Please input your password'}]
                        })(
                            <Input prefix={<Icon type="lock" />} placeholder="password" type="password" style={{ color: 'rgba(0,0,0,.25)' }} />
                        )}
                    </FormItem>
                    <FormItem>
                        <Checkbox className="checkbox">Remember me</Checkbox>
                        <a href="/login" className="login-form-forgot">forgot your password?</a>
                        <Button type="primary" htmlType="submit" className="login-form-login">Login</Button>
                        Or <a href="#">register now!</a>
                    </FormItem>
                </div>
            </Form>
            {isAuthenticated && <Redirect to="/" />}
        </div>
    )
}

const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
})

const mapDispatchToProps = (dispatch) => ({
    loginRequest: (email, password) => dispatch(loginRequest(email, password))
})

export default connect(mapStateToProps, mapDispatchToProps)(Form.create()(Login))