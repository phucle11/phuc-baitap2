import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import Login from './containers/Login/Login'
import ListItem from './containers/ListItem/ListItem'
import * as serviceWorker from './serviceWorker'

import { BrowserRouter as Router, Switch} from 'react-router-dom'
import PrivateRoute from './layouts/PrivateLayout'
import PublicRoute from './layouts/PublicLayout'

import { Provider } from 'react-redux'
import store from './redux/store'

ReactDOM.render(
    <Router>
        <Provider store={store}>
            <Switch>
                <PublicRoute path="/login/" exact component={Login} />
                <PrivateRoute path="/" exact component={ListItem} />
            </Switch>
        </Provider>
    </Router>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
